const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/


// Q1. Find all the items with price more than $65.

// console.log("......................test1......................")

let priceMoreThan65 = products.map(product => {

    return Object.fromEntries(Object.entries(product).filter(item => {

        if (!item[1].price) {


            let result = item[1].filter(item => {

                let ok = Object.entries(item).filter(item => {

                    return item[1].price.replace('$', "") > 65
                })

                if (ok.length != 0) {
                    return ok
                }

            })

            console.log(result)


        } else {
            return item[1].price > '$65'
        }
    }))
})
console.log(priceMoreThan65)


// Q2. Find all the items where quantity ordered is more than 1.

// console.log("......................test2......................")


let quantityMoreThanOne = products.map(product => {

    return Object.fromEntries(Object.entries(product).filter(item => {

        if (!item[1].price) {


            let result = item[1].filter(item => {

                let ok = Object.entries(item).filter(item => {

                    return item[1].quantity > 1
                })

                if (ok.length != 0) {
                    return ok
                }

            })

            console.log(result)


        } else {
            return item[1].quantity > 1
        }
    }))
})
console.log(quantityMoreThanOne)







// Q.3 Get all items which are mentioned as fragile.


let itemsWithFragile = products.map(product => {

    return Object.fromEntries(Object.entries(product).filter(item => {

        if (!item[1].price) {


            let result = item[1].filter(item => {

                let ok = Object.entries(item).filter(item => {

                    return item[1].type == 'fragile'
                })

                if (ok.length != 0) {
                    return ok
                }

            })

            console.log(result)


        } else {
            return item[1].type == 'fragile'
        }
    }))
})

console.log(itemsWithFragile)


// console.log(itemsWithFragile)